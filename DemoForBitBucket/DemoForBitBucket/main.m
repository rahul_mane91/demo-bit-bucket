//
//  main.m
//  DemoForBitBucket
//
//  Created by Rahul V. Mane on 12/19/13.
//  Copyright (c) 2013 Perennial. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
