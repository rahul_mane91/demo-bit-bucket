//
//  AppDelegate.h
//  DemoForBitBucket
//
//  Created by Rahul V. Mane on 12/19/13.
//  Copyright (c) 2013 Perennial. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
